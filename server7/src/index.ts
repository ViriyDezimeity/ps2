import express from "express";
import db from './db';
import bodyParser from 'body-parser'
import { IUserRequest } from './interfaces/requestInterfaces'

const app = express();

app.use(bodyParser.json());

//#region Users

app.get("/users", async function(request, response){
    const res = { 
      users: (await db.query('SELECT * FROM users')).rows
    }
    response.send(res);
});

app.post("/users", async (req, res) => {
  const jsonRequest: IUserRequest = req.body;
  console.log(jsonRequest)
  if (jsonRequest.firstname && jsonRequest.lastname && jsonRequest.patronymic && jsonRequest.placework && jsonRequest.phonenumber && jsonRequest.mail && jsonRequest.login && jsonRequest.password && jsonRequest.aboutme) {
    await db.query(`INSERT INTO Users (FirstName, LastName, Patronymic, PlaceWork, PhoneNumber, Mail, Login, Password, AboutMe) VALUES ('${jsonRequest.firstname}', '${jsonRequest.lastname}', '${jsonRequest.patronymic}', '${jsonRequest.placework}', '${jsonRequest.phonenumber}', '${jsonRequest.mail}', '${jsonRequest.login}', '${jsonRequest.password}', '${jsonRequest.aboutme}')`)
      .then((result) => {
        if (result.rowCount !== 0) {
          res.send({
            firstname: jsonRequest.firstname,
            lastname: jsonRequest.lastname,
            patronymic: jsonRequest.patronymic,
            placework: jsonRequest.placework,
            phonenumber: jsonRequest.phonenumber,
            mail: jsonRequest.mail,
            login: jsonRequest.login,
            password: jsonRequest.password,
            aboutme: jsonRequest.aboutme,
            isAdded: true,
          });
        };
      })
      .catch((reason) => {
        console.error(reason);
        res.send({
          isAdded: false,
        });
      });
  } else {
    console.error("Некоторые поля не заполнены");
    res.status(400).send({
      isAdded: false,
    })
  }
})

app.put("/users", async (req, res) => {
  const jsonRequest: IUserRequest = req.body;

  if (jsonRequest.id) {
    await db.query(`UPDATE Users SET FirstName = '${jsonRequest.firstname}', LastName = '${jsonRequest.lastname}', Patronymic = '${jsonRequest.patronymic}', PlaceWork = '${jsonRequest.placework}', PhoneNumber = '${jsonRequest.phonenumber}', Mail = '${jsonRequest.mail}', Login = '${jsonRequest.login}', Password = '${jsonRequest.password}', AboutMe = '${jsonRequest.aboutme}' WHERE userid = ${jsonRequest.id}`)
      .then((result) => {
        if (result.rowCount !== 0) {
          res.send({
            id: jsonRequest.id,
            isUpdated: true,
          });
        };
      })
      .catch((reason) => {
        console.error(reason);
        res.send({
          id: jsonRequest.id,
          isUpdated: false,
        });
      });
  } else {
    console.error("Некоторые поля не заполнены");
    res.status(400).send({
      id: jsonRequest.id,
      isUpdated: false,
    })
  }
})

app.delete("/users", async (req, res) => {
  const jsonRequest: IUserRequest = req.body;

  if (jsonRequest.id) {
    await db.query(`DELETE FROM users WHERE userid = ${jsonRequest.id}`)
      .then((result) => {
        if (result.rowCount !== 0) {
          res.send({
            id: jsonRequest.id,
            isDeleted: true,
          });
        };
      })
      .catch((reason) => {
        console.error(reason);
        res.send({
          id: jsonRequest.id,
          isDeleted: false,
        });
      });
  } 
  
  
  
  else {
    console.error("Некоторые поля не заполнены");
    res.status(400).send({
      id: jsonRequest.id,
      isDeleted: false,
    })
  }
})

//#endregion

app.get("/projects", async function(request, response){
  const res = { 
    projects: (await db.query('SELECT * FROM projects')).rows
  }
  response.send(res);
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server started on ${PORT}`);
});
