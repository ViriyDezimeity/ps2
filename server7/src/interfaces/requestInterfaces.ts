export interface IUserRequest {
  id?: number,
  firstname?: string,
  lastname?: string,
  patronymic?: string,
  placework?: string,
  phonenumber?: string,
  mail?: string,
  login?: string,
  password?: string,
  aboutme?: string,
}